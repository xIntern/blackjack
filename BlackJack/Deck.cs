﻿using System;
using System.Collections.Generic;

namespace BlackJack
{
    public class Deck
    {
        public List<Card> Cards;
        public Deck()
        {
            Cards = new List<Card>();
            foreach (Suit suit in Enum.GetValues(typeof(Suit)))
            {
                for (int i = 1; i < 14; i++)
                {
                    var value = i < 10 ? i : 10;
                    var name = "";
                    if (i == 1) name = "A";
                    if (i == 11) name = "J";
                    if (i == 12) name = "Q";
                    if (i == 13) name = "K";
                    Cards.Add(new Card() { Rank = value, Suit = suit, Name = name });
                }
            }

        }

        public List<Card> DealCards(int num)
        {
            var returnCards = new List<Card>();
            var random = new Random();
            for (int i = 0; i < num; i++)
            {
                int randomCardNum = random.Next(0, Cards.Count);
                returnCards.Add(Cards[randomCardNum]);
                Cards.RemoveAt(randomCardNum);
            }
            return returnCards;
        }

        public Card DealOneCard()
        {
            return DealCards(1)[0];
        }
    }
}
