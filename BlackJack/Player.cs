﻿using BlackJack;
using System;
using System.Collections.Generic;

public class Player
{
    public string Name { get; set; }
    public List<Card> Hand = new List<Card>();

    public Player() {}
	public Player(string name)
	{
        Name = name;
	}

    public void AddCardsToHand(List<Card> cards)
    {
        cards.ForEach(c =>
        {
            if (c.Rank == 1 && GetHandScore() <= 10) c.Rank = 11;
            Hand.Add(c);
        });
    }
    public void AddCardToHand(Card card)
    {
        AddCardsToHand(new List<Card> { card });
    }

    public int GetHandScore()
    {
        var sum = 0;
        Hand.ForEach(c => sum += c.Rank);
        return sum;
    }

    public void ShowHand()
    {
        Console.WriteLine("{0}'s hand:", Name);
        Hand.ForEach(c => Console.WriteLine("{0} of {1}", c.GetName(), c.Suit));
    }
}
