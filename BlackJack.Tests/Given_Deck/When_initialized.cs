﻿using System;
using System.Linq;
using NUnit.Framework;

namespace BlackJack.Tests.Given_Deck
{
    public class When_initialized : Scenario
    {
        private Deck _deck;
        private Player _player;
        public override void When()
        {
            _deck = new Deck();
            _player = new Player();
        }

        [Test]
        public void Should_have_52_cards()
        {
            Assert.AreEqual(52, _deck.Cards.Count);
        }

        [Test]
        public void Should_have_4_distinct_suits()
        {
            Assert.AreEqual(4, _deck.Cards.Select(x => (int)x.Suit).Distinct().ToList().Count);
        }

        [Test]
        public void Should_be_of_card_instance()
        {
            Assert.IsInstanceOf(new Card().GetType(), _deck.DealOneCard());
        }

        [Test]
        public void Should_get_two_cards()
        {
            Assert.AreEqual(2, _deck.DealCards(2).Count);
        }

        [Test]
        public void Should_have_removed_three_cards()
        {
            _deck.DealCards(3);
            Assert.AreEqual(49, _deck.Cards.Count);
        }

        [Test]
        public void Should_be_empty_hand()
        {
            Assert.AreEqual(0, _player.Hand.Count);
        }

        [Test]
        public void Should_add_a_card_to_players_hand()
        {
            Assert.AreEqual(0, _player.Hand.Count);
            _player.AddCardToHand(new Card());
            Assert.AreEqual(1, _player.Hand.Count);
        }

        [Test]
        public void Should_set_player_name()
        {
            string name = "Andreas";
            _player.Name = name;
            Assert.AreEqual(name, _player.Name);
        }

        [Test]
        public void Should_have_a_hand_score()
        {
            _player.AddCardToHand(_deck.DealOneCard());
            Assert.Greater(_player.GetHandScore(), 0);
        }

        [Test]
        public void Should_get_a_cards_name()
        {
            Assert.IsNotEmpty(_deck.DealOneCard().GetName());
        }
    }
}
