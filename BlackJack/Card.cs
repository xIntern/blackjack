﻿namespace BlackJack
{
    public class Card
    {
        public Suit Suit;
        public int Rank;
        public string Name;

        public string GetName()
        {
            return Name.Length > 0 ? Name : Rank.ToString();
        }
    }
}
