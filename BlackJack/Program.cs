﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BlackJack
{
    class Program
    {
        static void Main(string[] args)
        {
            var maxScore = 0;
            var deck = new Deck();
            var dealer = new Player("Dealer");
            var player = new Player("Player");
            var playing = true;

            while (playing)
            {
                if (dealer.Hand.Count == 0) dealer.AddCardsToHand(deck.DealCards(2));
                if (player.Hand.Count == 0) player.AddCardsToHand(deck.DealCards(2));

                Console.WriteLine("Dealer has {0} of {1}", dealer.Hand[0].GetName(), dealer.Hand[0].Suit);
                player.ShowHand();
                Console.WriteLine("With a total of {0} points", player.GetHandScore());

                Console.WriteLine();

                if (player.GetHandScore() == 21)
                {
                    Console.WriteLine("You got 21! You win!");
                    dealer.ShowHand();
                }

                Console.WriteLine("Would you like to stand or hit?");
                string action = Console.ReadLine().ToLower();

                if (action == "hit")
                {
                    var hitCard = deck.DealOneCard();
                    player.AddCardToHand(hitCard);
                    Console.WriteLine("Hit with {0} of {1}. Total is {2}", hitCard.GetName(), hitCard.Suit, player.GetHandScore());

                    if (player.GetHandScore() > 21)
                    {
                        Console.WriteLine("You lost with {0} points", player.GetHandScore());
                        maxScore = 0;
                        playing = false;
                    }
                }
                else if (action == "stand")
                {
                    Console.WriteLine("You stand with {0} points", player.GetHandScore());
                    if (player.GetHandScore() > 21)
                    {
                        Console.WriteLine("You lost with {0} points", player.GetHandScore());
                        dealer.ShowHand();
                        maxScore = 0;
                        playing = false;
                    }

                    while (dealer.GetHandScore() < 17)
                    {
                        var newCard = deck.DealOneCard();
                        dealer.AddCardToHand(newCard);
                        Console.WriteLine("Dealer drew a new card {0} of {1}", newCard.GetName(), newCard.Suit);
                        dealer.ShowHand();
                        Console.WriteLine("With a total of {0} points", dealer.GetHandScore());
                    }
                    Console.WriteLine();
                    if (player.GetHandScore() > dealer.GetHandScore() || dealer.GetHandScore() > 21)
                    {
                        dealer.ShowHand();
                        Console.WriteLine("Dealer got {0} points", dealer.GetHandScore());
                        Console.WriteLine("You won!");
                        maxScore++;
                        playing = false;
                    }
                    if (player.GetHandScore() < dealer.GetHandScore() && dealer.GetHandScore() <= 21)
                    {
                        dealer.ShowHand();
                        Console.WriteLine("Dealer got {0} points", dealer.GetHandScore());
                        Console.WriteLine("You loose");
                        maxScore = 0;
                        playing = false;
                    }
                    if (player.GetHandScore() == dealer.GetHandScore())
                    {
                        Console.WriteLine("Its a draw!");
                        dealer.ShowHand();
                        Console.WriteLine("You and the dealer both got {0} points", dealer.GetHandScore());
                        maxScore = 0;
                        playing = false;
                    }
                }
                if (!playing)
                {
                    if (maxScore > 0) Console.WriteLine("Your highscore is {0}", maxScore);
                    Console.WriteLine("Play again? (y/n)");
                    var pressedKey = Console.ReadKey();
                    if (pressedKey.KeyChar == 'y')
                    {
                        deck = new Deck();
                        dealer = new Player("Dealer");
                        player = new Player("Player");
                        playing = true;
                        Console.Clear();
                    }
                }
            }
        }
    }
}
